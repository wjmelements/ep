PL=swipl -L0
EXEC=ep
all:
	$(PL) -o $(EXEC) -c english.pl 
run:
	./$(EXEC)
clean:
	rm -f $(EXEC)
test:
	cat test-cases | $(PL) -f english.pl
