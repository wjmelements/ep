% prolog

%lexer
lex(String, Tokens) :-
    string(Tokens, String, []).

string([Word]) --> word(Word), end.
string([Word | String]) --> word(Word), space, string(String).
string([Word, ','| String]) --> word(Word), comma, space, string(String).
space -->  [32]. % space  
comma --> [44]. % comma ,
end --> [46]. % period .
end --> [33]. % bang !
word(Word) --> word(Word, []).
word(Word, Acc) --> letter(Letter), {append(Acc, [Letter], Letters), name(Word, Letters)}.
word(Word, Acc) --> letter(Letter), {append(Acc, [Letter], Letters)}, word(Word, Letters).
:- consult(alphabet). % letters

% grammar

sentence(Sentence) --> independent(Sentence).
sentence(comp(Conjunction, Sentence1, Sentence2)) --> independent(Sentence1), [Conjunction], {conjunction(Conjunction)}, sentence(Sentence2).
sentence(nom(Dependent, Sentence)) --> dependent(Dependent), [','], sentence(Sentence).
sentence(nom(Dependent, Sentence)) --> dependent(Dependent), sentence(Sentence).
dependent(PrepositionalPhrase) --> prepositionalPhrase(PrepositionalPhrase).
dependent(VerbPhrase) --> verbPhrase(VerbPhrase).
dependent(spec(Specifier, Independent)) --> [Specifier], {specifier(Specifier)}, independent(Independent).
independent(ic(NounPhrase, VerbPhrase)) --> nounPhrase(NounPhrase), verbPhrase(VerbPhrase).

nounPhrase(Noun) --> item(Noun).
nounPhrase(listing(Join,Nouns)) --> listing(Join,Nouns).
nounPhrase(union(Noun1,Noun2)) --> item(Noun1), [','], item(Noun2).
nounPhrase(union(Noun1,Noun2)) --> item(Noun1), [','], item(Noun2), [','].
nounPhrase(union(Noun,listing(Join,Nouns))) --> item(Noun), [','], listing(Join,Nouns).
nounPhrase(union(Noun,listing(Join,Nouns))) --> item(Noun), [','], listing(Join,Nouns), [','].
nounPhrase(union(listing(Join,Nouns), Noun)) --> listing(Join, Nouns), [','], item(Noun).
nounPhrase(union(listing(Join,Nouns), Noun)) --> listing(Join, Nouns), [','], item(Noun), [','].
nounPhrase(union(listing(Join1, Nouns1), listing(Join2, Nouns2))) --> listing(Join1, Nouns1), [','], listing(Join2, Nouns2).
nounPhrase(union(listing(Join1, Nouns1), listing(Join2, Nouns2))) --> listing(Join1, Nouns1), [','], listing(Join2, Nouns2), [','].
listing(Join,[Item|Nouns]) --> item(Item), [','], listing(Join,Nouns).
listing(Join,[Item1,Item2,Item3]) --> item(Item1), [','], item(Item2), [','], [Join], {joiner(Join)}, item(Item3).
listing(Join,[Item1,Item2]) --> item(Item1), [Join], {joiner(Join)}, item(Item2).
item(Noun) --> nominal(Noun).
item(nom(Article,Noun)) --> [Article], {article(Article)}, nominal(Noun).
nominal(Noun) --> [Noun], {noun(Noun)}.
nominal(nom(Adj,Noun)) --> adjPhrase(Adj), nominal(Noun).
nominal(Tree) --> [Noun], {noun(Noun)}, left_assoc_pp(Noun, Tree).
left_assoc_pp(Pushed, Tree) --> prepositionalPhrase(Right), left_assoc_pp(nom(Pushed, Right), Tree).
left_assoc_pp(Noun, nom(Prep, Noun)) --> prepositionalPhrase(Prep).
prepositionalPhrase(pp(Preposition, NounPhrase)) --> [Preposition], {preposition(Preposition)}, nounPhrase(NounPhrase).
adjPhrase(Adj) --> [Adj], {adjective(Adj)}.
adjPhrase(ap(Adverb,Adjective)) --> [Adverb], {adverb(Adverb)}, adjPhrase(Adjective).
verbPhrase(VerbPhrase) --> verbI(VerbPhrase).
verbPhrase(vp(VerbPhrase, Object)) --> verbI(VerbPhrase), nounPhrase(Object).
verbPhrase(vp(Tree, Object)) --> verbI(VerbPhrase), nounPhrase(Object), left_assoc_pp(VerbPhrase, Tree).
verbI(Verb) --> [Verb], {verb(Verb)}.
verbI(nom(Adverb,Verb)) --> [Adverb], {adverb(Adverb)}, verbI(Verb).
verbI(Tree) --> [Verb], {verb(Verb)}, left_assoc_verbi(Verb, Tree).
verbI(Tree) --> [Verb], {verb(Verb)}, left_assoc_pp(Verb, Tree).
left_assoc_verbi(Pushed, Tree) --> [Adverb], {adverb(Adverb)}, left_assoc_verbi(nom(Adverb, Pushed), Tree).
left_assoc_verbi(Verb, nom(Adverb, Verb)) --> [Adverb], {adverb(Adverb)}.

% terminals
:- consult(dict).

% AST
parse(Words, AST) :-
    sentence(AST, Words, []).
process(String, AST) :-
    lex(String, Tokens),
    parse(Tokens, AST).
