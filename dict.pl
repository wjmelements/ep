% prolog
article(a).
article(an).
article(her).
article(his).
article(my).
article(the).
article(our).

joiner(and).
joiner(or).

conjunction(and).
conjunction(but).
conjunction(for).
conjunction(nor).
conjunction(or).
conjunction(so).
conjunction(yet).

adjective(bear).
adjective(black).
adjective(dark).
adjective(exquisite).
adjective(fat).
adjective(hard).

adverb(hard).
adverb(not).
adverb(quickly).
adverb(very).

preposition(about).
preposition(along).
preposition(at).
preposition(behind).
preposition(by).
preposition(despite).
preposition(down).
preposition(during).
preposition(from).
preposition(in).
preposition(on).
preposition(past).
preposition(save).
preposition(since).
preposition(through).
preposition(to).
preposition(up).
preposition(with).

specifier(when).

noun(arms).
noun(bear).
noun(box).
noun(dog).
noun(furlough).
noun(girl).
noun(he).
noun(hitler).
noun(house).
noun(i).
noun(it).
noun(man).
noun(nest).
noun(past).
noun(jewels).
noun(rain).
noun(rifle).
noun(sea).
noun(seas).
noun(sniper).
noun(stalin).
noun(strippers).
noun(we).
noun(work).

verb(barked).
verb(bear).
verb(fly).
verb(furlough).
verb(invited).
verb(pour).
verb(pours).
verb(puts).
verb(ran).
verb(rains).
verb(runs).
verb(see).
verb(sees).
verb(shot).
verb(works).


